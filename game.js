
/*

TODO



Idées de règles :

Move / Defend / Shoot
Forme + couleur ?
plus de caractéristiques ?
Plusieurs actions par tour (fixe ou extensible par bonus?)
Unités upgradables (en killant ou manuellement)


*/

var playerGrid = [];  // player
var ennemyGrid = []; // ennemy

var pieces = [];

var seedRandom = 0;


var TYPE_PIECE = {

    'Empty' : 0,

    'Frog' : 1,
    'Plane' : 2,
    'Swan' : 3,

    'BackFrog' : 4,
    'BackPlane' : 5,
    'BackSwan' : 6
};


var caseClicked = null;
function onMouseDown(pointer, gameObject)
{
    //console.log(pointer);
    //console.log(gameObject);

    //console.log(gameObject.coords);

    var c = playerGrid[gameObject.coords[0]][gameObject.coords[1]];

    if(c != null)
    {
        selectType(playerGrid, c.type);
        caseClicked = gameObject.coords;
    }
}

function onMouseUp(pointer, gameObject)
{
    var drop = playerGrid[gameObject.coords[0]][gameObject.coords[1]];
    var drag = null;
    if (caseClicked != null) drag = playerGrid[caseClicked[0]][caseClicked[1]];

    //console.log(caseClicked);
    //console.log(gameObject.coords);

    if(drop != null && caseClicked != null && drag != null)
    {
        // can we move ?
        if(/*drop.type == TYPE_PIECE.Empty &&*/ drag.type != TYPE_PIECE.Empty)
        {

            //if (gameObject.coords[0] == caseClicked[0] - 1 && gameObject.coords[1] == caseClicked[1]) // left move
            if (gameObject.coords[0] <caseClicked[0] && gameObject.coords[1] == caseClicked[1]) // left move
            {
                moveType(playerGrid, drag.type, -1, 0);
            }

            //else if (gameObject.coords[0] == caseClicked[0] + 1 && gameObject.coords[1] == caseClicked[1]) // right move
            else if (gameObject.coords[0] > caseClicked[0] && gameObject.coords[1] == caseClicked[1]) // right move
            {
                moveType(playerGrid, drag.type, 1, 0);
            }

            //else if (gameObject.coords[1] == caseClicked[1] - 1 && gameObject.coords[0] == caseClicked[0]) // top move
            else if (gameObject.coords[1] < caseClicked[1] && gameObject.coords[0] == caseClicked[0]) // top move
            {
                moveType(playerGrid, drag.type, 0, -1);
            }

            //else if (gameObject.coords[1] == caseClicked[1] + 1 && gameObject.coords[0] == caseClicked[0]) // bottom move
            else if (gameObject.coords[1] > caseClicked[1] && gameObject.coords[0] == caseClicked[0]) // bottom move
            {
                moveType(playerGrid, drag.type, 0, 1);
            }

        }
    }

    caseClicked = null;
    //deselectAll(playerGrid);
}

function onPointerUp(pointer)
{
    deselectAll(playerGrid);
}


// clear all cases and reset the game's variables
function clearGame()
{
    for(var j = 0 ; j < gameWidth ; ++j) // for each col
    {
        for(var i = 0 ; i < gameHeight ; ++i) // for each line
        {
            clearCase(playerGrid, j, i);
            clearCase(ennemyGrid, j, i);
        }
    }
}


// generate the grids for a new game
function generateGrids(difficulty, seed)
{
    clearGame();

    seedRandom = seed;

    /*var test = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    for(var r = 0 ; r < 100000 ; ++r){

        test[randomInt(9)]++;
    }
    console.log(test);*/

    ennemyGrid = generateGrid(difficulty, 20, false);
    playerGrid = generateGrid(difficulty, screenHeight - sizeCases - 20, true);
}

function generateGrid(difficulty, offset, interactive)
{
    var grid = [];

    for(var j = 0 ; j < gameWidth ; ++j) // for each col
    {
        grid[j] = [];

        for(var i = 0 ; i < gameHeight ; ++i) // for each line
        {
            var posX = j * sizeCase + (screenWidth - sizeCases) / 2;
            var posY = i * sizeCase + offset;

            grid[j][i] = generateCase(randomInt(3 + 2), posX, posY, j, i, interactive);

        }
    }

    return grid;
}

// generate a case
function generateCase(type, posX, posY, x, y, interactive, flagMoved = false)
{

    var c;

    if(type == TYPE_PIECE.Empty || type > /*TYPE_PIECE.length() - 1*/ 3)
        c = {'type': TYPE_PIECE.Empty,
             'img': phaserScene.add.image(posX, posY, 'case').setScale(screenScale / caseScale).setOrigin(0, 0),
             'flagMoved': flagMoved};

    if(type == TYPE_PIECE.Frog) c = {'type': TYPE_PIECE.Frog,
                                        'img': phaserScene.add.image(posX, posY, 'frog').setScale(screenScale / caseScale).setOrigin(0, 0),
                                        'img2': phaserScene.add.image(posX, posY, 'frog2').setScale(screenScale / caseScale).setOrigin(0, 0).setVisible(false),
                                        'flagMoved' : flagMoved};

    else if(type == TYPE_PIECE.Plane) c = {'type': TYPE_PIECE.Plane,
                                        'img': phaserScene.add.image(posX, posY, 'plane').setScale(screenScale / caseScale).setOrigin(0, 0),
                                        'img2': phaserScene.add.image(posX, posY, 'plane2').setScale(screenScale / caseScale).setOrigin(0, 0).setVisible(false),
                                        'flagMoved' : flagMoved};

    else if(type == TYPE_PIECE.Swan) c = {'type': TYPE_PIECE.Swan,
                                        'img': phaserScene.add.image(posX, posY, 'swan').setScale(screenScale / caseScale).setOrigin(0, 0),
                                        'img2': phaserScene.add.image(posX, posY, 'swan2').setScale(screenScale / caseScale).setOrigin(0, 0).setVisible(false),
                                        'flagMoved' : flagMoved};
     
    
    
    if(c.img != null)
    {
        c.img.coords = [x, y];
        if(interactive /*&& type != TYPE_PIECE.Empty*/) c.img.setInteractive()
    }

    if(c.img2 != null)
    {
        c.img2.coords = [x, y];
        if(interactive /*&& type != TYPE_PIECE.Empty*/) c.img2.setInteractive()
    }

    c.coords = [x, y];
                                        
    return c;
}


// clear a case (destroy both images)
function clearCase(grid, x, y) {

    if(grid[x] != null && grid[x][y] != null)
    {
        if(grid[x][y].img != null) grid[x][y].img.destroy();
        if(grid[x][y].img2 != null) grid[x][y].img2.destroy();

        grid[x][y] = null;
    }

}



// deselect all cases
function deselectAll(grid)
{
    for(var j = 0 ; j < gameWidth ; ++j) // for each col
    {
        for(var i = 0 ; i < gameHeight ; ++i) // for each line
        {
            deselectCase(grid, j, i);
        }
    }
}

// select a case
function selectCase(grid, x, y)
{
    if(grid[x] != null && grid[x][y] != null && grid[x][y].type != TYPE_PIECE.Empty)
    {
        if(grid[x][y].img != null) grid[x][y].img.setVisible(false);
        if(grid[x][y].img2 != null) grid[x][y].img2.setVisible(true);
    }

}

// deselect a case
function deselectCase(grid, x, y)
{
    if(grid[x] != null && grid[x][y] != null && grid[x][y].type != TYPE_PIECE.Empty)
    {
        if(grid[x][y].img != null) grid[x][y].img.setVisible(true);
        if(grid[x][y].img2 != null) grid[x][y].img2.setVisible(false);
    }

}


// select the cases of a type
function selectType(grid, type)
{
    for(var j = 0 ; j < gameWidth ; ++j) // for each col
    {
        for(var i = 0 ; i < gameHeight ; ++i) // for each line
        {
            if(type == grid[j][i].type)
            {
                selectCase(grid, j, i);
            }
        }
    }
}

// get all cases of a type
function getAllOfType(grid, type)
{
    var ret = [];

    for(var j = 0 ; j < gameWidth ; ++j) // for each col
    {
        for(var i = 0 ; i < gameHeight ; ++i) // for each line
        {
            if(type == grid[j][i].type)
            {
                //ret.push([j, i]);
                ret.push(grid[j][i]);
            }
        }
    }

    return ret;
}


function moveType(grid, type, dirX, dirY)
{
    for(var j = 0 ; j < gameWidth ; ++j) // for each col
    {
        for(var i = 0 ; i < gameHeight ; ++i) // for each line
        {
            if(grid[j][i] != null)
            {
                grid[j][i].flagMoved = false;
            }
        }
    }

    var list = getAllOfType(grid, type);

    alreadyMoved = [];

    list.forEach(function(c)
    {
        //console.log(c);

        if(type == c.type) {

            var x = c.coords[0];
            var y = c.coords[1];

            console.log("MOVING " + x + ";" + y + " => " + (x + dirX) + ";" + (y + dirY));

            moveCase(grid, x, y, dirX, dirY, type);
        }
    });
}


function moveCase(grid, x, y, dirX, dirY)
 {
    // we check that the block hasn't already be moved this turn
     if(grid[x][y].flagMoved) return;

    var type = grid[x][y].type;

    var oldPosX = (x) * sizeCase + (screenWidth - sizeCases) / 2;
    var oldPosY = (y) * sizeCase + (screenHeight - sizeCases - 20);
    var posX = (x + dirX) * sizeCase + (screenWidth - sizeCases) / 2;
    var posY = (y + dirY) * sizeCase + (screenHeight - sizeCases - 20);

    if((y + dirY >= 0) && (y + dirY < gameHeight) && (x + dirX >= 0) && (x + dirX < gameWidth))
    {
        // case not empty!
        if(grid[x + dirX][y + dirY] != null && grid[x + dirX][y + dirY].type != TYPE_PIECE.Empty)
        {
            moveCase(grid, x + dirX, y + dirY, dirX, dirY);
        }

        clearCase(grid, x + dirX, y + dirY);
        grid[x + dirX][y + dirY] = generateCase(type, posX, posY, x + dirX, y + dirY, true /*not for enemy?*/, true);

    }

    else
    {

    }

    clearCase(grid, x, y);
    grid[x][y] = generateCase(TYPE_PIECE.Empty, oldPosX, oldPosY, x, y, true /*not for enemy?*/);
 }


// https://stackoverflow.com/questions/521295/seeding-the-random-number-generator-in-javascript
function random()
{
    var x = Math.sin(seedRandom++) * 10000;
    return x - Math.floor(x);
}

function randomInt(max)
{
    return Math.floor(random() * (max + 1));  
}


