
    // *** configuration ***

    // w*h cases
    var gameWidth = 6;
    var gameHeight = 6;

    // reference resolution
    var screenWidthRef = 1080;
    var screenHeightRef = 1920;

    // use this to scale the game and all the elements
    var screenScale = 0.4;

    // scaled resolution
    var screenWidth = screenWidthRef * screenScale;
    var screenHeight = screenHeightRef * screenScale;

    var caseScale = 1.75;
    var sizeCase = 256 * screenScale / caseScale;
    var sizeCases = sizeCase * gameWidth;
